from cozydb import CozyStore
import json
from io import StringIO
import numpy as np

store = CozyStore(host='localhost', port=3306, user='root', passwd='9okm8ijn76',
                  db='test')
cursor = store.get_cursor()
cursor.execute('select value from moistdata')
showt = cursor.fetchall(as_dict=True)
numel = len(showt)
print "Number of elements are : ",numel
cnt = 0

bl_par = -2
hl_par = 80
inc = 0.1

val = np.zeros(numel)
err = np.zeros(numel)
errd = np.zeros(numel)
asmp = np.ones(numel)
minerr = np.ones((hl_par-bl_par)*(hl_par-bl_par)*1000)
minslp = np.ones((hl_par-bl_par)*(hl_par-bl_par)*1000)
minint = np.ones((hl_par-bl_par)*(hl_par-bl_par)*1000)
minerr = minerr*10
minint = minint*10
minslp = minslp*10

cnt = 0
while(cnt<numel):
	bug = showt[cnt]
	val[cnt] = int(bug['value'])
	#print val[cnt]
	cnt = cnt+1

tot = sum(val)
avg = tot/numel
print"Sum of all elements is : ",tot
print"Average of all elements is : ",avg

cnt = 0

while(cnt<numel):
	bug = showt[cnt]
	err[cnt] =  abs(val[cnt]-avg)
	cnt = cnt+1

toterr = sum(err)
avgerr = toterr/numel
print"Average error of all elements is : ",avgerr

cnt = 0

while(cnt<numel):
	asmp[cnt] = cnt+1
	cnt = cnt+1

#print(val)

slope = bl_par;
inter = bl_par;

while(cnt<numel):
	asmp[cnt] = cnt+1
	cnt = cnt+1

cnt = 0
mcnt = 0
while inter<hl_par:
	while slope<hl_par:
		while(cnt<numel):
			asmp[cnt] = cnt+1
			cnt = cnt+1
		cnt = 0
		asmp = (slope*asmp)
		asmp = asmp+inter
		while cnt<numel:
			errd[cnt] = abs(asmp[cnt]-val[cnt])
			cnt = cnt+1
		cnt = 0
		toterr = sum(errd)
		avgerr = toterr/numel
		toterr = 0
		minerr[mcnt] = avgerr
		minslp[mcnt] = slope
		minint[mcnt] = inter
		mcnt = mcnt+1
		slope = slope + inc
		#if avgerr<2:
			#print 'For slope : ',slope,' and intercept : ',inter,' error is : ',avgerr
		avgerr = 0
	inter = inter + inc
	slope = bl_par

minind = np.argmin(minerr)
minind = int(minind)
#print minerr
#print type(minind)
print 'Minimum Error is ',minerr[minind]
print 'Minimum Slope is ',minslp[minind]
print 'Minimum Intercept is ',minint[minind]
print 'Last value is ', val[numel-1]
print 'Predicted Value is', ((minslp[minind]*numel+1)+minint[minind])

while(cnt<numel):
	asmp[cnt] = cnt+1
	cnt = cnt+1
asmp = (minslp[minind]*asmp)
asmp = asmp+minint[minind]
print(asmp)
cnt = 0
while cnt<numel:
	errd[cnt] = abs(asmp[cnt]-val[cnt])
	cnt = cnt+1
cnt = 0
toterr = sum(errd)
avgerr = toterr/numel
#print avgerr
#numpy.argmin